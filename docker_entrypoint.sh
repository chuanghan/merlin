#!/usr/bin/env bash

echo APP_ENV: $APP_ENV

./download_checkpoints.sh

echo 'Starting gunicorn...'
exec gunicorn --workers 1 --bind 0.0.0.0:5003 --timeout 180 run:app
