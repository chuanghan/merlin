#!/usr/bin/env bash

docker run -p 8501:8501 \
  --mount type=bind,source=$(pwd)/models/,target=/models/ \
  -e MODEL_NAME=polygon -t tensorflow/serving