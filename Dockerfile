FROM python:2.7

ADD ./ /src
WORKDIR /src
RUN pip install -r requirements.txt

ENV APP_ENV=prod
EXPOSE 5003
ENTRYPOINT ["sh", "docker_entrypoint.sh"]