#!/usr/bin/env bash

POLYGON_CHECKPOINTS_DIR=checkpoints/polygon
POLYGON_CHECKPOINTS_TMP=tmp/polygon_checkpoint.tar.gz
POLYGON_CHECKPOINTS_URL=http://stardust-public.oss-cn-hangzhou.aliyuncs.com/polygon_checkpoint.tar.gz

echo 'Download checkpoints...'
if [ ! -d "$POLYGON_CHECKPOINTS_DIR" ]; then
    echo "Download [Polygon] checkpoints..."
    curl $POLYGON_CHECKPOINTS_URL --create-dirs -o $POLYGON_CHECKPOINTS_TMP
    mkdir -p $POLYGON_CHECKPOINTS_DIR
    tar -xvf $POLYGON_CHECKPOINTS_TMP -C $POLYGON_CHECKPOINTS_DIR
    rm $POLYGON_CHECKPOINTS_TMP
fi
