# -*- coding: utf-8 -*-

from flask import Flask
from flask_cors import CORS


def create_app():
    flask_app = Flask(__name__)
    CORS(flask_app)
    setup_blueprints(flask_app)
    return flask_app


def setup_blueprints(flask_app):
    """
    Flask蓝图模式，用来管理路由
    :param flask_app:
    :return:
    """
    from segmentation.polygon.api import blueprint as polygon

    flask_app.register_blueprint(polygon, url_prefix='/merlin')


app = create_app()


@app.route('/')
def index():
    return "Welcome"
