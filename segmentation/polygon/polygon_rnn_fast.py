import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

from polygon_model import PolygonModel
from StringIO import StringIO
from poly_utils import vis_polys
from PIL import Image
import tensorflow as tf
import os
import numpy as np
import skimage.io as io

#
tf.logging.set_verbosity(tf.logging.WARN)
# --
flags = tf.flags
FLAGS = flags.FLAGS
# ---
flags.DEFINE_string('PolyRNN_metagraph', 'checkpoints/polygon/models/poly/polygonplusplus.ckpt.meta', 'PolygonRNN++ MetaGraph ')
flags.DEFINE_string('PolyRNN_checkpoint', 'checkpoints/polygon/models/poly/polygonplusplus.ckpt', 'PolygonRNN++ checkpoint ')
flags.DEFINE_string('EvalNet_checkpoint', 'checkpoints/polygon/models/evalnet/evalnet.ckpt', 'Evaluator checkpoint ')
flags.DEFINE_string('GGNN_metagraph', 'checkpoints/polygon/models/ggnn/ggnn.ckpt.meta', 'GGNN poly MetaGraph ')
flags.DEFINE_string('GGNN_checkpoint', 'checkpoints/polygon/models/ggnn/ggnn.ckpt', 'GGNN poly checkpoint ')
flags.DEFINE_boolean('Use_ggnn', False, 'Use GGNN to postprocess output')

#

_BATCH_SIZE = 1
_FIRST_TOP_K = 1

# The acceptable image size fro segmentation-rnn
POLYGON_RNN_IMG_SIZE = 224

dir(tf.contrib)


class PolygonRNN(object):
    def __init__(self):
        # Creating the graphs
        self.eval_graph = tf.Graph()
        self.poly_graph = tf.Graph()

        # PolygonRNN++
        self.model = PolygonModel(FLAGS.PolyRNN_metagraph, self.poly_graph)

        self.poly_session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True), graph=self.poly_graph)
        self.model.saver.restore(self.poly_session, FLAGS.PolyRNN_checkpoint)

    def inference(self, image_data):
        processed_image_data = PolygonRNN._pre_process_input(image_data)
        image_np = io.imread(StringIO(processed_image_data))
        image_np = np.expand_dims(image_np, axis=0)
        preds = [self.model.do_test(self.poly_session, image_np, top_k) for top_k in range(_FIRST_TOP_K)]

        return preds[0]['polys'][0]

    def visualize_predction(self, image_file):
        preds = self.inference(PolygonRNN.get_image_data(image_file))

        fig, axes = plt.subplots(1, 1, num=0, figsize=(12, 6))
        axes = np.array(axes).flatten()
        import ipdb; ipdb.set_trace()
        im_crop, polys = io.imread(image_file), np.array(preds)
        vis_polys(axes[0], im_crop, polys, title='PolygonRNN++ : %s ' % image_file)

        fig_name = os.path.join('output/', image_file).replace('imgs/', '')
        fig.savefig(fig_name)
        [ax.cla() for ax in axes]

    @staticmethod
    def get_image_data(file_name):
        with open(file_name, 'r') as f:
            data = f.read()
        return data

    @staticmethod
    def _pre_process_input(image_data):
        image = Image.open(StringIO(image_data))

        # resize image to expected size
        resized_image = image.resize((POLYGON_RNN_IMG_SIZE, POLYGON_RNN_IMG_SIZE))

        # remove alpha channel in the image
        if resized_image.mode in ('RGBA', 'LA') or (resized_image.mode == 'P' and 'transparency' in resized_image.info):
            resized_image.load()

            background = Image.new('RGB', resized_image.size, (255, 255, 255))
            background.paste(resized_image, mask=resized_image.split()[3])
            resized_image = background

        output = StringIO()
        resized_image.save(output, 'JPEG', quality=80)
        resized_image_data = output.getvalue()
        output.close()

        return resized_image_data
