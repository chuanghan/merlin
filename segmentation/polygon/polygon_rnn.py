import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

from polygon_model import PolygonModel
from eval_net import EvalNet
from ggnn_poly_model import GGNNPolygonModel
from StringIO import StringIO
from poly_utils import vis_polys
from PIL import Image
import tensorflow as tf
import os
import numpy as np
import utils
import skimage.io as io
import json
import requests

#
tf.logging.set_verbosity(tf.logging.WARN)
# --
flags = tf.flags
FLAGS = flags.FLAGS
# ---
flags.DEFINE_string('PolyRNN_metagraph', 'checkpoints/polygon/models/poly/polygonplusplus.ckpt.meta', 'PolygonRNN++ MetaGraph ')
flags.DEFINE_string('PolyRNN_checkpoint', 'checkpoints/polygon/models/poly/polygonplusplus.ckpt', 'PolygonRNN++ checkpoint ')
flags.DEFINE_string('EvalNet_checkpoint', 'checkpoints/polygon/models/evalnet/evalnet.ckpt', 'Evaluator checkpoint ')
flags.DEFINE_string('GGNN_metagraph', 'checkpoints/polygon/models/ggnn/ggnn.ckpt.meta', 'GGNN poly MetaGraph ')
flags.DEFINE_string('GGNN_checkpoint', 'checkpoints/polygon/models/ggnn/ggnn.ckpt', 'GGNN poly checkpoint ')
flags.DEFINE_boolean('Use_ggnn', True, 'Use GGNN to postprocess output')

#

_BATCH_SIZE = 1
_FIRST_TOP_K = 1

# The acceptable image size fro segmentation-rnn
POLYGON_RNN_IMG_SIZE = 224


class PolygonRNN(object):
    def __init__(self):
        # Creating the graphs
        self.eval_graph = tf.Graph()
        self.poly_graph = tf.Graph()

        # Evaluator Network
        with self.eval_graph.as_default():
            with tf.variable_scope("discriminator_network"):
                evaluator = EvalNet(_BATCH_SIZE)
                evaluator.build_graph()
            saver = tf.train.Saver()

            # Start session
            eval_session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True), graph=self.eval_graph)
            saver.restore(eval_session, FLAGS.EvalNet_checkpoint)

        # PolygonRNN++
        self.model = PolygonModel(FLAGS.PolyRNN_metagraph, self.poly_graph)
        self.model.register_eval_fn(lambda input_: evaluator.do_test(eval_session, input_))

        self.poly_session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True), graph=self.poly_graph)
        self.model.saver.restore(self.poly_session, FLAGS.PolyRNN_checkpoint)

        if FLAGS.Use_ggnn:
            self.ggnn_graph = tf.Graph()
            self.ggnn_model = GGNNPolygonModel(FLAGS.GGNN_metagraph, self.ggnn_graph)
            self.ggnn_session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True), graph=self.ggnn_graph)

            self.ggnn_model.saver.restore(self.ggnn_session, FLAGS.GGNN_checkpoint)

    def inference(self, image_data):
        processed_image_data = PolygonRNN._pre_process_input(image_data)
        image_np = io.imread(StringIO(processed_image_data))
        image_np = np.expand_dims(image_np, axis=0)
        preds = [self.model.do_test(self.poly_session, image_np, top_k) for top_k in range(_FIRST_TOP_K)]

        # sort predictions based on the eval score and pick the best
        preds = sorted(preds, key=lambda x: x['scores'][0], reverse=True)[0]

        if FLAGS.Use_ggnn:
            polys = np.copy(preds['polys'][0])
            feature_indexs, poly, mask = utils.preprocess_ggnn_input(polys)
            preds_gnn = self.ggnn_model.do_test(self.ggnn_session, image_np, feature_indexs, poly, mask)
            return preds_gnn['polys_ggnn'][0]
        else:
            return preds['polys'][0]

    def visualize_predction(self, image_file):
        preds = self.inference(PolygonRNN.get_image_data(image_file))

        fig, axes = plt.subplots(1, 1, num=0, figsize=(12, 6))
        axes = np.array(axes).flatten()
        im_crop, polys = io.imread(image_file), np.array(preds)
        vis_polys(axes[0], im_crop, polys, title='PolygonRNN++ : %s ' % image_file)

        fig_name = os.path.join('output/', image_file).replace('imgs/', '')
        fig.savefig(fig_name)
        [ax.cla() for ax in axes]

    @staticmethod
    def get_image_data(file_name):
        with open(file_name, 'r') as f:
            data = f.read()
        return data

    @staticmethod
    def _pre_process_input(image_data):
        image = Image.open(StringIO(image_data))

        # resize image to expected size
        resized_image = image.resize((POLYGON_RNN_IMG_SIZE, POLYGON_RNN_IMG_SIZE))

        # remove alpha channel in the image
        if resized_image.mode in ('RGBA', 'LA') or (resized_image.mode == 'P' and 'transparency' in resized_image.info):
            resized_image.load()

            background = Image.new('RGB', resized_image.size, (255, 255, 255))
            background.paste(resized_image, mask=resized_image.split()[3])
            resized_image = background

        output = StringIO()
        resized_image.save(output, 'JPEG', quality=80)
        resized_image_data = output.getvalue()
        output.close()

        return resized_image_data

    @staticmethod
    def get_request():
        data = PolygonRNN.get_image_data('resize.png')
        processed_data = PolygonRNN._pre_process_input(data)
        image_np = io.imread(StringIO(processed_data))
        image_np = np.expand_dims(image_np, axis=0).astype(np.float32).tolist()
        response = requests.post("http://localhost:8501/v1/models/polygon:predict", data=json.dumps({'instances': [{'InputImgs': image_np, 'TopKFirstPoint': 1}]}))
        return response
