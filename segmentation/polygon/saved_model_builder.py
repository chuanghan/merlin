import tensorflow as tf
from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import signature_constants
from tensorflow.python.saved_model import signature_def_utils
from tensorflow.python.saved_model import tag_constants
from tensorflow.python.saved_model.utils import build_tensor_info

dir(tf.contrib)

_FIRST_TOP_K = 1
# The acceptable image size fro segmentation-rnn
_IMG_SIZE = 224

tf.flags.DEFINE_string('PolyRNN_metagraph', 'checkpoints/polygon/models/poly/polygonplusplus.ckpt.meta', 'PolygonRNN++ MetaGraph ')
tf.flags.DEFINE_string('PolyRNN_checkpoint', 'checkpoints/polygon/models/poly/polygonplusplus.ckpt', 'PolygonRNN++ checkpoint ')
FLAGS = tf.flags.FLAGS

# eval_graph = tf.Graph()
poly_graph = tf.Graph()

image_bytes = tf.placeholder(tf.string, shape=[], name="image_bytes")

with poly_graph.as_default():
    poly_saver = tf.train.import_meta_graph(FLAGS.PolyRNN_metagraph, clear_devices=True)
    poly_session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True), graph=poly_graph)
    poly_saver.restore(poly_session, FLAGS.PolyRNN_checkpoint)

    image_bytes = tf.reshape(image_bytes, [])
    # Transform bitstring to uint8 tensor
    image_tensor = tf.image.decode_png(image_bytes, channels=3)
    image_tensor = tf.image.convert_image_dtype(image_tensor,  dtype=tf.int32)
    image_tensor = tf.reshape(image_tensor, [_IMG_SIZE, _IMG_SIZE, 3])
    # Expand the single tensor into a batch of 1
    image_tensor = tf.expand_dims(image_tensor, 0)

    poly_outputs = {
        'polys': poly_graph.get_tensor_by_name('OutputPolys:0'),
        'masks': poly_graph.get_tensor_by_name('OutputMasks:0'),
        'state1': poly_graph.get_tensor_by_name('OutputState1:0'),
        'state2': poly_graph.get_tensor_by_name('OutputState2:0'),
        'cnn_feats': poly_graph.get_tensor_by_name('OutputCNNFeats:0')
    }

    tensor_imgs = poly_session.graph.get_tensor_by_name('InputImgs:0')
    tensor_top_k = poly_session.graph.get_tensor_by_name('TopKFirstPoint:0')
    tensor_polys = poly_session.graph.get_tensor_by_name('OutputPolys:0')
    tensor_masks = poly_session.graph.get_tensor_by_name('OutputMasks:0')

    input_imgs = build_tensor_info(tensor_imgs)
    input_top_k = build_tensor_info(tensor_top_k)
    output_polys = build_tensor_info(tensor_polys)
    output_masks = build_tensor_info(tensor_masks)

    # Create a signature definition for TensorFlow serving
    signature_definition = signature_def_utils.build_signature_def(
        inputs={'InputImgs': input_imgs, 'TopKFirstPoint': input_top_k},
        outputs={'OutputPolys': output_polys, 'OutputMasks': output_masks},
        method_name=signature_constants.PREDICT_METHOD_NAME)

    builder = saved_model_builder.SavedModelBuilder('./models/polygon/1')

    builder.add_meta_graph_and_variables(poly_session, [tag_constants.SERVING], signature_def_map={
        signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: signature_definition})

    builder.save()
