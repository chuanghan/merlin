from base64 import b64decode

from flask import Blueprint, jsonify, request

from polygon_rnn_fast import PolygonRNN

blueprint = Blueprint('segmentation', __name__)

polygon_client = PolygonRNN()


@blueprint.route('polygon', methods=['POST'])
def inference():
    data = request.get_json()

    if data is None or not data.get('image'):
        raise Exception(message="Bad request", status_code=400)

    image = b64decode(data.get('image'))

    polys = polygon_client.inference(image).tolist()

    return jsonify(polys)


@blueprint.route('health', methods=['GET'])
def health():
    return "OK"
